class AddSuperappToOauthApplication < ActiveRecord::Migration[5.2]
  def change
    add_column :oauth_applications, :superapp, :boolean, default: false, null: false
  end
end
