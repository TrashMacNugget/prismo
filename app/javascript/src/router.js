import Vue from 'vue'
import VueRouter from 'vue-router'

import StoriesRoute from './routes/StoriesRoute.vue'
import StoryRoute from './routes/StoryRoute.vue'
import StoriesHotRoute from './routes/stories/HotRoute.vue'
import StoriesRecentRoute from './routes/stories/RecentRoute.vue'

import CommentsRoute from './routes/CommentsRoute.vue'
import CommentRoute from './routes/CommentRoute.vue'
import CommentsRecentRoute from './routes/comments/RecentRoute.vue'

import TagRoute from './routes/TagRoute.vue'
import TagStoriesHotRoute from './routes/tag/StoriesHotRoute.vue'
import TagStoriesRecentRoute from './routes/tag/StoriesRecentRoute.vue'

import AccountRoute from './routes/AccountRoute.vue'
import AccountStoriesRoute from './routes/account/StoriesRoute.vue'
import AccountCommentsRoute from './routes/account/CommentsRoute.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: StoriesRoute,
    children: [
      { path: '', name: 'stories', component: StoriesHotRoute, meta: { navMainLink: 'stories' } },
      { path: 'recent', name: 'storiesRecent', component: StoriesRecentRoute, meta: { navMainLink: 'stories' } }
    ]
  },
  { path: '/stories/:id', name: 'story', component: StoryRoute, meta: { navMainLink: 'stories' } },

  {
    path: '/comments',
    component: CommentsRoute,
    children: [
      { path: '', name: 'comments', component: CommentsRecentRoute, meta: { navMainLink: 'comments' } },
    ]
  },
  { path: '/comments/:id', name: 'comment', component: CommentRoute, meta: { navMainLink: 'comments' } },
  {
    path: '/tags/:name',
    component: TagRoute,
    children: [
      { path: '', name: 'tag', component: TagStoriesHotRoute },
      { path: 'recent', name: 'tagStoriesRecent', component: TagStoriesRecentRoute },
    ]
  },
  {
    path: '/@:username',
    component: AccountRoute,
    children: [
      { path: '', name: 'account', component: AccountStoriesRoute },
      { path: 'comments', name: 'accountComments', component: AccountCommentsRoute },
    ]
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
