import { Model } from '@vuex-orm/core'

export default class Account extends Model {
  static entity = 'accounts'

  static fields() {
    return {
      id: this.attr(null),
      is_admin: this.attr(false),
      username: this.attr(''),
      display_name: this.attr(''),
      bio: this.attr(''),
      avatar_urls: this.attr({}),
    }
  }
}
