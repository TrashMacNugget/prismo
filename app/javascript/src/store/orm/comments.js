import * as api from '../../api'
import _map from 'lodash/map'

export default {
  actions: {
    fetchAll (context, { payload }) {
      context.dispatch('backend/setIsLoading', { key: 'comments', isLoading: true }, { root: true })

      let req = api.fetchComments(payload)

      req.then((resp) => {
        context.dispatch('entities/comments/create', { data: resp.data }, { root: true })
        context.dispatch('backend/setPagination', { headers: resp.headers, key: 'comments' }, { root: true })
        context.dispatch('backend/setIsLoading', { key: 'comments', isLoading: false }, { root: true })
      })
    },

    fetchOne(context, payload) {
      let req = api.fetchComment({ id: payload.id })

      req.then((resp) => {
        context.dispatch('entities/comments/create', { data: resp.data }, { root: true })
      })
    },

    fetchAllForStory (context, { payload }) {
      context.dispatch('backend/setIsLoading', { key: 'storyComments', isLoading: true }, { root: true })

      let req = api.fetchAllStoryComments(payload)

      req.then((resp) => {
        let comments = _map(resp.data, (comment) => {
          return Object.assign({}, comment, {
            closed: comment.depth > 2
          })
        })

        context.dispatch('entities/comments/create', { data: comments }, { root: true })
        context.dispatch('backend/setIsLoading', { key: 'storyComments', isLoading: false }, { root: true })
      })
    },

    fetchAllChildren(context, { payload }) {
      context.dispatch('backend/setIsLoading', { key: 'commentChildren', isLoading: true }, { root: true })

      let req = api.fetchAllCommentChildren(payload)
      req.then((resp) => {
        context.dispatch('entities/comments/insert', { data: resp.data }, { root: true })
        context.dispatch('backend/setIsLoading', { key: 'commentChildren', isLoading: false }, { root: true })
      })

      return req
    },

    upvote (context, { comment }) {
      let req = api.createVoteOnComment({ comment_id: comment.id })

      req.then((resp) => {
        context.dispatch('entities/comments/insert', { data: resp.data.voteable }, { root: true })
      })
    },

    unvote(context, { comment }) {
      let req = api.destroyVoteOnComment({ comment_id: comment.id })

      req.then((resp) => {
        context.dispatch('entities/comments/insert', { data: resp.data.voteable }, { root: true })
      })
    },

    createOnBackend(context, payload) {
      let params = { parent_id: payload.parentId, body: payload.body }

      let req = (payload.parentType == 'Comment')
        ? api.createCommentReply(params)
        : api.createStoryComment(params)

      req.then((resp) => {
        context.dispatch('entities/comments/insert', { data: resp.data }, { root: true })
      })

      return req
    },

    updateOnBackend(context, payload) {
      let params = { id: payload.id, body: payload.body }
      let req = api.updateComment(params)

      req.then((resp) => {
        context.dispatch('entities/comments/insert', { data: resp.data }, { root: true })
      })

      return req
    },

    setClosed (context, { comment, closed }) {
      context.dispatch('entities/comments/update', {
        where: comment.id,
        data: { closed: closed }
      }, { root: true })
    }
  }
}
