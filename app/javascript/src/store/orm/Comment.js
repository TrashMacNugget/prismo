import { Model } from '@vuex-orm/core'
import Account from './Account'
import Story from './Story'

export default class Comment extends Model {
  static entity = 'comments'

  static fields() {
    return {
      id: this.attr(null),
      body: this.attr(''),
      body_html: this.attr(''),
      children_count: this.attr(0),
      created_at: this.attr(null),
      depth: this.attr(null),
      is_voted: this.attr(false),
      votes_count: this.attr(null),
      story_id: this.attr(null),
      parent_id: this.attr(null),
      account_id: this.attr(null),
      closed: this.attr(false),
      account: this.belongsTo(Account, 'account_id'),
      story: this.belongsTo(Story, 'story_id'),
    }
  }
}
