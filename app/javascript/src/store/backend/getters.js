import _sortBy from 'lodash/sortBy'

export default {
  currentAccount(state) {
    return state.accounts[state.meta.current_account_id] || null
  },

  storiesByHottness(state) {
    let stories = []
    let gravity = 1.8

    Object.keys(state.stories).forEach((key) => {
      stories.push(state.stories[key])
    })

    stories = _sortBy(stories, (story) => {
      let hourAge = (Date.now() - Date.parse(story.created_at)) / (1000 * 3600)
      return -((story.votes_count - 1) / Math.pow(hourAge + 2, gravity))
    })

    return stories
  },

  storiesByRecentness(state) {
    let stories = []

    Object.keys(state.stories).forEach((key) => {
      stories.push(state.stories[key])
    })

    stories = _sortBy(stories, (story) => {
      return -story.created_at
    })

    return stories
  },

  tagsByTaggingsCount(state) {
    let tags = []

    Object.keys(state.tags).forEach((key) => {
      tags.push(state.tags[key])
    })

    tags = _sortBy(tags, (tag) => {
      return -tag.taggings_count
    })

    return tags
  },

  comments(state) {
    let comments = []

    Object.keys(state.comments).forEach((key) => {
      comments.push(state.comments[key])
    })

    return comments
  },

  commentsByRecentness(state) {
    let comments = []

    Object.keys(state.comments).forEach((key) => {
      comments.push(state.comments[key])
    })

    comments = _sortBy(comments, (story) => {
      return -story.created_at
    })

    return comments
  },

  commentsByHottness(state) {
    let comments = []
    let gravity = 1.8

    Object.keys(state.comments).forEach((key) => {
      comments.push(state.comments[key])
    })

    comments = _sortBy(comments, (comment) => {
      let hourAge = (Date.now() - Date.parse(comment.created_at)) / (1000 * 3600)
      return -((comment.votes_count - 1) / Math.pow(hourAge + 2, gravity))
    })

    return comments
  },
}
