import _map from 'lodash/map'
import * as types from '../mutation-types'
import * as api from '../../api'
import Axios from 'axios'

import Story from '../orm/Story'

export default {
  handleInitialState({ commit }, initialState) {
    commit(types.BACKEND_SET_META, { meta: initialState.meta })
    commit(types.BACKEND_RECEIVE_DATA, {
      data: initialState.accounts,
      key: 'accounts'
    })
  },

  setIsLoading({ commit }, payload) {
    commit(types.BACKEND_SET_IS_LOADING, payload)
  },

  setPagination({ commit }, payload) {
    commit(types.BACKEND_SET_PAGINATION, payload)
  },

  clearStories({ commit }) {
    commit(types.BACKEND_CLEAR_STORIES)
  },

  clearComments({ commit }) {
    commit(types.BACKEND_CLEAR_COMMENTS)
  },

  fetchTags({ commit }) {
    let req = api.fetchTags()

    req.then((resp) => {
      commit(types.BACKEND_RECEIVE_DATA, {
        data: resp.data,
        key: 'tags'
      })
    })
  },

  fetchStories({ commit, dispatch }, { payload, overwrite = true, clearBefore = false }) {
    if (clearBefore) { commit(types.BACKEND_CLEAR_STORIES) }

    commit(types.BACKEND_SET_IS_LOADING, { key: 'stories', isLoading: true })

    let req = api.fetchStories(payload)

    req.then((resp) => {
      if (overwrite) { commit(types.BACKEND_CLEAR_STORIES) }

      commit(types.BACKEND_RECEIVE_DATA, {
        data: resp.data,
        key: 'stories'
      })

      commit(types.BACKEND_SET_PAGINATION, {
        headers: resp.headers,
        key: 'stories'
      })

      commit(types.BACKEND_SET_IS_LOADING, { key: 'stories', isLoading: false })
    })
  },

  fetchStory({ commit }, payload) {
    commit(types.BACKEND_SET_IS_LOADING, { key: 'story', isLoading: true })

    let req = api.fetchStory(payload)

    req.then((resp) => {
      commit(types.BACKEND_RECEIVE_DATA, {
        data: [resp.data],
        key: 'stories'
      })

      commit(types.BACKEND_SET_IS_LOADING, { key: 'story', isLoading: false })
    })
  },

  fetchAccount({ commit }, payload) {
    let req = api.fetchAccount(payload)

    req.then((resp) => {
      commit(types.BACKEND_RECEIVE_DATA, {
        data: [resp.data],
        key: 'accounts'
      })
    })
  },

  upvoteStory({ commit }, payload) {
    let req = api.createVoteOnStory(payload)

    req.then((resp) => {
      commit(types.BACKEND_RECEIVE_DATA, {
        data: [resp.data.voteable],
        key: 'stories'
      })
    })
  },

  unvoteStory({ commit }, payload) {
    let req = api.destroyVoteOnStory(payload)

    req.then((resp) => {
      commit(types.BACKEND_RECEIVE_DATA, {
        data: [resp.data.voteable],
        key: 'stories'
      })
    })
  },

  receiveData({ commit }, payload) {
    commit(types.BACKEND_RECEIVE_DATA, payload)
  }
}
