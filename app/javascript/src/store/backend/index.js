import getters from './getters'
import actions from './actions'
import mutations from './mutations'

// Default module's state
const state = {
  comments: {},
  tags: {},
  stories: {},
  accounts: {},
  meta: {},

  pagination: {
    stories: { nextPage: 1, prevPage: null },
    comments: { nextPage: 1, prevPage: null },
  },

  isLoading: {
    stories: false,
    story: false,
    comments: false,
    storyComments: false,
    commentChildren: false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
