import * as types from '../mutation-types'
import Vue from 'vue'

function parsePaginationHeader(val) {
  return (val == '') ? null : parseInt(val)
}

export default {
  [types.BACKEND_SET_META](state, { meta }) {
    Vue.set(state, 'meta', meta)
  },

  [types.BACKEND_CLEAR_STORIES](state) {
    state.stories = {}
  },

  [types.BACKEND_CLEAR_COMMENTS](state) {
    state.comments = {}
  },

  [types.BACKEND_RECEIVE_DATA](state, { data, key, close = false, onlyUpdate = false }) {
    data.forEach((n) => {
      if (key == 'comments') {
        n.isClosed = close && n.depth > 2
        n.isReplyOpened = false
      }

      if (onlyUpdate) {
        if (state[key][n.id]) { Vue.set(state[key], n.id, n) }
      } else {
        Vue.set(state[key], n.id, n)
      }
    })
  },

  [types.BACKEND_SET_PAGINATION](state, { headers, key }) {
    Vue.set(state.pagination, key, {
      totalPages: parsePaginationHeader(headers['x-pagination-total-pages']),
      nextPage: parsePaginationHeader(headers['x-pagination-next-page']),
      prevPage: parsePaginationHeader(headers['x-pagination-prev-page']),
      page: parsePaginationHeader(headers['x-pagination-page']),
      totalCount: parsePaginationHeader(headers['x-pagination-total-count'])
    })
  },

  [types.BACKEND_SET_COMMENT_IS_CLOSED](state, { comment, isClosed }) {
    state.comments[comment.id].isClosed = isClosed
  },

  [types.BACKEND_SET_COMMENT_IS_REPLY_OPENED](state, { comment, isReplyOpened }) {
    state.comments[comment.id].isReplyOpened = isReplyOpened
  },

  [types.BACKEND_SET_IS_LOADING](state, { key, isLoading }) {
    Vue.set(state.isLoading, key, isLoading)
  }
}
