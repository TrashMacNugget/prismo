import * as types from '../mutation-types'
import Vue from 'vue'

export default {
  [types.UI_SHOW_SIGN_IN_MODAL](state, payload) {
    Vue.set(state, 'showSignInModal', payload)
  },

  [types.UI_SHOW_ADD_STORY_MODAL](state, payload) {
    Vue.set(state, 'showAddStoryModal', payload)
  },
}
