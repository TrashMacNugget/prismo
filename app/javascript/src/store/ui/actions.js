import * as types from '../mutation-types'

export default {
  showSignInModal({ commit }, payload) {
    commit(types.UI_SHOW_SIGN_IN_MODAL, payload)
  },

  showAddStoryModal({ commit }, payload) {
    commit(types.UI_SHOW_ADD_STORY_MODAL, payload)
  },
}
