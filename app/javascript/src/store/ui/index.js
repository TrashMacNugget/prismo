// import getters from './getters'
import actions from './actions'
import mutations from './mutations'

// Default module's state
const state = {
  showSignInModal: false,
  showAddStoryModal: false,
}

export default {
  namespaced: true,
  state,
  // getters,
  actions,
  mutations
}
