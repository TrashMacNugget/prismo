import Vue from 'vue'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'

import AuthPlugin from './plugins/auth'
import ActioncablePlugin from './plugins/actioncable'
import Database from './plugins/orm'

import backend from './backend'
import ui from './ui'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  modules: {
    backend, ui
  },

  plugins: [
    AuthPlugin,
    ActioncablePlugin,
    VuexORM.install(Database)
  ]
})

export default store
