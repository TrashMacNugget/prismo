import ActionCable from 'actioncable'

let ws = ActionCable.createConsumer("ws://localhost:3000/cable")

const actioncablePlugin = store => {
  ws.subscriptions.create('UpdatesChannel', {
    received(payload) {
      switch (payload.event) {
        // Listen for story changes and update them if already present in the store
        case 'stories.updated':
          store.dispatch('backend/receiveData', { data: [payload.data], key: 'stories', onlyUpdate: true })
          break;

        // Listen for comment changes and update them if already present in the store
        case 'comments.updated':
          store.dispatch('entities/comments/update', {
            where: payload.data.id,
            data: {
              body: payload.data.body,
              body_html: payload.data.body_html,
              children_count: payload.data.children_count,
            }
          })
          break;
      }
    }
  })
}

export default actioncablePlugin
