const authPlugin = store => {
  store.subscribe((mutation, state) => {
    // When user tries to open an add-new-story modal
    if (mutation.type == 'ui/UI_SHOW_ADD_STORY_MODAL' && mutation.payload) {

      // and if user is not signed in, display sign-in modal instead.
      if (state.backend.meta.current_account_id == null) {
        store.commit('ui/UI_SHOW_ADD_STORY_MODAL', false)
        store.commit('ui/UI_SHOW_SIGN_IN_MODAL', true)
        return false
      }
    }
  })
}

export default authPlugin
