import VuexORM from '@vuex-orm/core'

// Models and modules
import Story from '../orm/Story'
import Comment from '../orm/Comment'
import Account from '../orm/Account'
import stories from '../orm/stories'
import comments from '../orm/comments'
import accounts from '../orm/accounts'

// Create new instance of Database.
const database = new VuexORM.Database()

// Register models and modules
database.register(Story, stories)
database.register(Comment, comments)
database.register(Account, accounts)

export default database
