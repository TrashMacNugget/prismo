import Axios from 'axios'

export function fetchComment (payload = {}) {
  return Axios.get(`/api/v1/comments/${payload.id}`)
}

export function fetchComments(payload = {}) {
  return Axios.get(`/api/v1/comments`, payload)
}

export function fetchAllStoryComments(payload = {}) {
  return Axios.get(`/api/v1/stories/${payload.story.id}/comments/all`, payload)
}

export function fetchCommentChildren (payload = {}) {
  return Axios.get(`/api/v1/comments/${payload.comment.id}/comments`, {
    params: payload.params
  })
}

export function fetchAllCommentChildren(payload = {}) {
  return Axios.get(`/api/v1/comments/${payload.comment.id}/comments/all`, {
    params: payload.params
  })
}

export function fetchTags(payload = {}) {
  return Axios.get(`/api/v1/tags`)
}

export function fetchStories(payload = {}) {
  return Axios.get(`/api/v1/stories`, payload)
}

export function fetchStory (payload = {}) {
  return Axios.get(`/api/v1/stories/${payload.id}`)
}

export function scrapStory(payload = {}) {
  return Axios.post(`/api/v1/stories/${payload.id}/scrap`)
}

export function fetchAccount(payload = {}) {
  return Axios.get(`/api/v1/accounts/${payload.username}`)
}

export function createStoryComment (payload) {
  return Axios.post(`/api/v1/stories/${payload.parent_id}/comments`, {
    comment: { body: payload.body }
  })
}

export function createCommentReply(payload) {
  return Axios.post(`/api/v1/comments/${payload.parent_id}/comments`, {
    comment: { body: payload.body }
  })
}

export function updateComment(payload) {
  return Axios.put(`/api/v1/comments/${payload.id}`, {
    comment: { body: payload.body }
  })
}

export function createVoteOnStory(payload) {
  return Axios.post(`/api/v1/stories/${payload.story_id}/vote`)
}

export function destroyVoteOnStory(payload) {
  return Axios.post(`/api/v1/stories/${payload.story_id}/unvote`)
}

export function createVoteOnComment(payload) {
  return Axios.post(`/api/v1/comments/${payload.comment_id}/vote`)
}

export function destroyVoteOnComment(payload) {
  return Axios.post(`/api/v1/comments/${payload.comment_id}/unvote`)
}

export function metaScrapUrl (url) {
  return Axios.post(`/api/v1/tools/scrap_url`, {
    url: url
  })
}
