import Vue from 'vue'
import axios from 'axios'
import store from '../store'
import VueAxios from 'vue-axios'

// Attach authentication header
axios.interceptors.request.use((config) => {
  const token = store().state.backend.meta.token;

  if (token != null) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  return config;
})

// Handle backend response errors
axios.interceptors.response.use((response) => {
  return response
}, (error) => {
  switch (error.response.status) {
    case 401:
      store().dispatch('ui/showSignInModal', true)
      break

    default:
      return Promise.reject(error)
  }
})

Vue.use(VueAxios, axios)
