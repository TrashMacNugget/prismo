import ActionCable from 'actioncable'

let ws = ActionCable.createConsumer("ws://localhost:3000/cable")
let ActionCablePlugin = {}

ActionCablePlugin.install = (Vue, options) => {
  Vue.prototype.$cable = ws
}

export default ActionCablePlugin
