class StoryPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    user.present?
  end

  def update?
    user.present? && (user.is_admin? || user == record.account.user)
  end

  def scrap?
    user.present? && (user.is_admin? || user == record.account.user)
  end

  def comment?
    user.present?
  end
end
