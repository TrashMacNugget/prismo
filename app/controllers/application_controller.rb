class ApplicationController < ActionController::Base
  include Pundit

  before_action :set_initial_state_json

  helper_method :current_account
  helper_method :current_access_token

  def app; end

  private

  def current_account
    current_user&.account
  end

  def current_access_token
    @current_access_token ||= Users::FindOrCreateAccessToken.run(user: current_user).result
  end

  def initial_state_params
    OpenStruct.new(
      # settings: Web::Setting.find_by(user: current_user)&.data || {},
      # push_subscription: current_account.user.web_push_subscription(current_session),
      site_settings: Form::AdminSettings.new,
      current_account: current_account,
      token: current_access_token&.token
      # admin: Account.find_local(Setting.site_contact_username),
    )
  end

  def set_initial_state_json
    serializable_resource =
      ActiveModelSerializers::SerializableResource.new(initial_state_params, serializer: InitialStateSerializer)

    @initial_state_json = serializable_resource.to_json
  end
end
