class Api::V1::StoryCommentsController < Api::V1Controller
  before_action -> { doorkeeper_authorize! :write }, only: [:create]

  def index
    story = find_story
    comments = story.comments

    render json: comments, each_serializer: REST::CommentSerializer
  end

  def all
    voted_comment_ids = []

    comments = find_story.comments.includes(:account)
    voted_comment_ids = current_account.upvoted_comments.map(&:voteable_id) if user_signed_in?

    render json: comments,
           each_serializer: REST::CommentSerializer,
           exclude: [:story],
           voted_comment_ids: voted_comment_ids
  end

  def create
    story = find_story
    authorize story, :comment?

    outcome = Comments::Create.run(
      parent: story,
      body: params.fetch(:comment)[:body],
      account: current_user.account
    )

    if outcome.valid?
      render json: outcome.result, status: :created, serializer: REST::CommentSerializer
    else
      render json: outcome, serializer: REST::InteractionErrorSerializer, status: :bad_request
    end
  end

  private

  def find_story
    Story.find(params[:story_id])
  end

  def votes_for_story_by_account
    Vote
      .select(:voteable_id)
      .joins('LEFT JOIN comments ON comments.id = votes.voteable_id')
      .where(votes: { voteable_type: 'Comment', account_id: current_account.id })
  end

  def comment_params
    params.require(:comment).permit(:body, :parent_id)
  end
end
