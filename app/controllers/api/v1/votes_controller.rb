class Api::V1::VotesController < Api::V1Controller
  before_action -> { doorkeeper_authorize! :write }

  def create
    outcome = vote_interaction

    if outcome.valid?
      voteable = outcome.result.voteable
      render json: {
        vote: Serializer.new(outcome.result, serializer: REST::VoteSerializer),
        voteable: voteable_serializer(voteable),
      }, status: :created
    else
      render json: outcome,
             serializer: REST::InteractionErrorSerializer,
             status: :bad_request
    end
  end

  def destroy
    outcome = unvote_interaction

    if outcome.valid?
      voteable = outcome.result.voteable

      render json: {
        vote: Serializer.new(outcome.result, serializer: REST::VoteSerializer),
        voteable: voteable_serializer(voteable),
      }
    else
      render json: outcome,
             serializer: REST::InteractionErrorSerializer,
             status: :bad_request
    end
  end

  private

  def vote_interaction
    if params[:story_id].present?
      Stories::Vote.run(story: Story.find(params[:story_id]), account: current_user.account)
    else
      Comments::Vote.run(comment: Comment.find(params[:comment_id]), account: current_user.account)
    end
  end

  def unvote_interaction
    if params[:story_id].present?
      Stories::Unvote.run(story: Story.find(params[:story_id]), account: current_user.account)
    else
      Comments::Unvote.run(comment: Comment.find(params[:comment_id]), account: current_user.account)
    end
  end

  def voteable_serializer(voteable)
    serializer = voteable.is_a?(Story) ? REST::StorySerializer : REST::CommentSerializer

    Serializer.new(voteable, serializer: serializer,
                             scope: current_account,
                             scope_name: :current_account)
  end
end
