class Api::V1::CommentsController < Api::V1Controller
  def index
    comments = find_comments
    voted_comment_ids = current_account.upvoted_comments.map(&:voteable_id) if user_signed_in?

    set_pagination_headers(comments.results)

    render json: comments.results,
           each_serializer: REST::CommentSerializer,
           voted_comment_ids: voted_comment_ids
  end

  def all
    voted_comment_ids = []
    comment = find_comment

    voted_comment_ids = current_account.upvoted_comments.map(&:voteable_id) if user_signed_in?
    comments = comment.descendants.includes(:account)

    render json: comments,
           each_serializer: REST::CommentSerializer,
           exclude: [:story],
           voted_comment_ids: voted_comment_ids
  end

  def show
    comment = find_comment

    render json: comment, serializer: REST::CommentSerializer
  end

  def create
    comment = find_comment
    authorize comment, :comment?

    outcome = Comments::Create.run(
      parent: comment,
      body: params.fetch(:comment)[:body],
      account: current_user.account
    )

    if outcome.valid?
      render json: outcome.result, status: :created, serializer: REST::CommentSerializer
    else
      render json: outcome, serializer: REST::InteractionErrorSerializer, status: :bad_request
    end
  end

  def update
    comment = find_comment
    authorize comment, :update?

    outcome = Comments::Update.run(
      comment: comment,
      body: params.fetch(:comment)[:body]
    )

    if outcome.valid?
      render json: outcome.result, status: :created, serializer: REST::CommentSerializer
    else
      render json: outcome, serializer: REST::InteractionErrorSerializer, status: :bad_request
    end
  end

  private

  def find_comment
    comment_id = params[:comment_id] ? params[:comment_id] : params[:id]
    Comment.find(comment_id)
  end

  def find_comments
    scope = nil
    scope = CommentsQuery.new(find_comment.children).all if params[:comment_id].present?

    CommentSearch.new scope: scope,
                      filters: params[:f],
                      page: params[:page],
                      per_page: params[:per_page]
  end
end
