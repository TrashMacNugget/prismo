class Api::V1::StoriesController < Api::V1Controller
  before_action -> { doorkeeper_authorize! :write }, only: [:create]

  def index
    authorize Story
    voted_story_ids = []
    voted_story_ids = current_account.upvoted_stories
                                     .map(&:voteable_id) if user_signed_in?

    stories = StorySearch.new(filters: params[:f],
                              page: params[:page],
                              per_page: params[:per_page])

    set_pagination_headers(stories.results)

    render json: stories.results,
           each_serializer: REST::StorySerializer,
           voted_story_ids: voted_story_ids
  end

  def show
    story = Story.find(params[:id])
    authorize story

    render json: story, serializer: REST::StorySerializer
  end

  def create
    authorize Story, :create?

    story_params = params.fetch(:story, {}).merge(account: current_user.account)
    outcome = Stories::Create.run(story_params)

    if outcome.valid?
      render json: outcome.result, status: :created, serializer: REST::StorySerializer
    else
      render json: outcome, serializer: REST::InteractionErrorSerializer, status: :bad_request
    end
  end

  def update
    authorize Story, :update?

    story = Story.find(params[:id])
    story_params = params.fetch(:story, {}).merge(account: current_user.account, story: story)
    outcome = Stories::Update.run(story_params)

    if outcome.valid?
      render json: outcome.result, status: :ok, serializer: REST::StorySerializer
    else
      render json: outcome, serializer: REST::InteractionErrorSerializer, status: :bad_request
    end
  end

  def scrap
    story = Story.find(params[:id])
    authorize story, :scrap?

    outcome = Stories::Rescrap.run(story: story, user: current_user)

    if outcome.valid?
      render json: outcome.result, serializer: REST::StorySerializer
    else
      render json: outcome, serializer: REST::InteractionErrorSerializer, status: :bad_request
    end
  end

  def vote
    outcome = Stories::Vote.run(story: Story.find(params[:id]), account: current_user.account)

    if outcome.valid?
      render json: outcome.result,
             status: :created,
             include: 'voteable.account,voteable.url_meta,voteable.tags',
             serializer: REST::VoteSerializer
    else
      render json: outcome,
             serializer: REST::InteractionErrorSerializer,
             status: :bad_request
    end
  end

  def unvote
    outcome = Stories::Unvote.run(story: Story.find(params[:id]), account: current_user.account)

    if outcome.valid?
      render json: outcome.result,
             status: :created,
             include: 'voteable.account,voteable.url_meta,voteable.tags',
             serializer: REST::VoteSerializer
    else
      render json: outcome,
             serializer: REST::InteractionErrorSerializer,
             status: :bad_request
    end
  end
end
