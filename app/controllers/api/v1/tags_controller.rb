class Api::V1::TagsController < Api::V1Controller
  def index
    tags = TagSearch.new filters: params[:f]

    render json: tags.results, each_serializer: REST::Gutentag::TagSerializer
  end
end
