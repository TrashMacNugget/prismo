class Api::V1::AccountsController < Api::V1Controller
  def show
    account = Account.find_by!(username: params[:id])

    render json: account, serializer: REST::AccountSerializer
  end
end
