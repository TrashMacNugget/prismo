class CommentSearch
  include SearchObject.module(:kaminari, :enum)

  scope { CommentsQuery.new.all }

  # per_page 25
  per_page 1
  max_per_page 100

  option :order, enum: %w(ranking created_at_desc)

  option(:account_id) { |scope, value| scope.where(account_id: value) }
  option(:story_id) { |scope, value| scope.where(story_id: value) }
  option(:parent_id) { |scope, value| scope.where(parent_id: value) }
  option(:depth) { |scope, value| scope.find_all_by_generation(value) }

  def apply_order_with_ranking(scope)
    scope.order('ranking(votes_count, created_at::timestamp) DESC')
  end

  def apply_order_with_created_at_desc(scope)
    scope.order(created_at: :desc)
  end
end
