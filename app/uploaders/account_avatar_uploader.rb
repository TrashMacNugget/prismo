require "image_processing/mini_magick"

class AccountAvatarUploader < Shrine
  include ImageProcessing::MiniMagick

  plugin :processing
  plugin :versions

  process(:store) do |io, context|
    original = io.download
    pipeline = ImageProcessing::MiniMagick.source(original)

    size_60  = pipeline.resize_to_fill!(60, 60)
    size_400 = pipeline.resize_to_fill!(400, 400)

    original.close!

    {
      size_60: size_60,
      size_400: size_400
    }
  end
end
