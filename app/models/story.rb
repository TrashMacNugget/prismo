class Story < ApplicationRecord
  Gutentag::ActiveRecord.call self

  include StoryThumbUploader[:thumb]

  belongs_to :account
  has_many :comments
  belongs_to :url_meta, optional: true
  has_many :votes, as: :voteable
  has_many :group_stories
  has_many :groups, through: :group_stories

  validates :url, allow_blank: true, uniqueness: true
end
