class Comment < ApplicationRecord
  has_closure_tree

  belongs_to :story, counter_cache: true
  belongs_to :account
  has_many :votes, as: :voteable

  def cache_depth
    update_attributes(depth_cached: depth)
  end

  def cache_body
    update_attributes(
      body_html: Kramdown::Document.new(body, auto_ids: false, autolink: true).to_html
    )
  end
end
