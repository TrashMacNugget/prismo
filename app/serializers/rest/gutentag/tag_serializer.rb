class REST::Gutentag::TagSerializer < ActiveModel::Serializer
  attribute :id
  attribute :name
  attribute :taggings_count
end
