class REST::AccountSerializer < ActiveModel::Serializer
  attribute :id
  attribute :username
  attribute :display_name
  attribute :is_admin
  attribute :bio

  attribute :avatar_urls do
    {
      size_400: object.avatar_url(:size_400),
      size_60:  object.avatar_url(:size_60)
    }
  end
end
