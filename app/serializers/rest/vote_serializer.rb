class REST::VoteSerializer < ActiveModel::Serializer
  def self.serializer_for(model, _options)
    return REST::StorySerializer if model.class.is_a? Story
    return REST::CommentSerializer if model.class.is_a? Comment
    super
  end

  attribute :id
  attribute :voteable_type
  attribute :voteable_id
  attribute :account_id

  has_one :voteable
end
