class REST::StorySerializer < ActiveModel::Serializer
  include RoutingHelper
  include ActionView::Helpers::TextHelper

  attribute :id
  attribute :account_id
  attribute :title
  attribute :url
  attribute :votes_count
  attribute :comments_count
  attribute :tag_names
  attribute :domain
  attribute :created_at
  attribute :is_voted
  attribute :description
  attribute :description_html
  attribute :description_excerpt

  has_one :account
  has_one :url_meta
  has_many :tags, serializer: REST::Gutentag::TagSerializer

  attribute(:domain) { object.decorate.domain }
  attribute(:score) { object.decorate.score }

  attribute :thumb_urls do
    {
      size_200: (object.thumb_url(:size_200) if object.thumb_data?)
    }
  end

  def is_voted
    if @instance_options.key?(:voted_story_ids)
      @instance_options[:voted_story_ids].include? object.id
    elsif defined?(current_account) && current_account.present?
      ids = current_account.upvoted_stories.map(&:voteable_id)
      ids.include? object.id
    else
      false
    end
  end

  # @todo: Move that to decorator
  def description_html
    return nil if object.description.blank?
    Kramdown::Document.new(object.description).to_html
  end

  # @todo: Move that to decorator
  def description_excerpt
    return nil if object.description.blank?
    strip_tags(Kramdown::Document.new(object.description).to_html).truncate(100)
  end
end
