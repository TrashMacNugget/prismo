class Users::FindOrCreateAccessToken < ActiveInteraction::Base
  object :user

  DEFAULT_SCOPES = 'read write vote'.freeze

  def execute
    return existing_token_for_user if existing_token_for_user.present?

    pp existing_token_for_user

    Doorkeeper::AccessToken.create!(new_access_token_attrs)
  end

  private

  def new_access_token_attrs
    {
      application_id: superapp&.id,
      resource_owner_id: user.id,
      scopes: DEFAULT_SCOPES,
      expires_in: Doorkeeper.configuration.access_token_expires_in,
      use_refresh_token: Doorkeeper.configuration.refresh_token_enabled?
    }
  end

  def existing_token_for_user
    @existing_token_for_user ||=
      Doorkeeper::AccessToken.matching_token_for(superapp, user, DEFAULT_SCOPES)
  end

  def superapp
    @superapp ||= Doorkeeper::Application.find_by!(superapp: true)
  end
end
