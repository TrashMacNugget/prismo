class Stories::Update < Stories::CreateUpdateBase
  object :story

  def execute
    story.title = title if title?
    story.tag_names = tag_names if tag_names?
    story.description = description if description?

    if story.save
      after_story_save_hook(story)
    else
      errors.merge!(story.errors)
    end

    story
  end

  private

  def after_story_save_hook(story)
    Stories::ScrapJob.perform_later(story.id) if story.url_changed?
    Stories::BroadcastChanges.run! story: story
  end
end
