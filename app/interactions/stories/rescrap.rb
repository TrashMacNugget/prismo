class Stories::Rescrap < ActiveInteraction::Base
  object :story
  object :user

  validate :scrap_rate_limit

  def execute
    if story.valid?
      Stories::ScrapJob.perform_later(story.id)
    else
      errors.merge!(story.errors)
    end

    story
  end

  private

  def scrap_rate_limit
    return if story.scrapped_at.nil?
    return if user.is_admin?

    too_early = (Time.zone.now - story.scrapped_at) < 10.minutes
    errors.add(:base, 'Story can be scrapped once per 10 minutes') if too_early
  end
end
