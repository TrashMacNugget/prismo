class Stories::Unvote < ActiveInteraction::Base
  object :story
  object :account

  def execute
    vote = ::Vote.find_by(account: account, voteable: story)
    vote.destroy if vote.present?

    # Stories::BroadcastChanges.run! story: story.reload

    vote
  end
end
