class Stories::CreateUpdateBase < ActiveInteraction::Base
  object :account

  string :url, default: nil
  string :title
  array :tag_names
  string :description, default: nil

  validates :url, url: { allow_blank: true }
  validate :url_or_description_required
  validate :min_tags_amount

  private

  def url_or_description_required
    return if url.present? || description.present?

    errors.add(:base, 'URL or description required')
  end

  def min_tags_amount
    return if Setting.min_story_tags.zero?
    return if tag_names.length >= Setting.min_story_tags

    errors.add(:tag_names, "needs to have at least #{Setting.min_story_tags} tags selected")
  end
end
