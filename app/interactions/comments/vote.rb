class Comments::Vote < ActiveInteraction::Base
  object :comment
  object :account

  def to_model
    ::Vote.new
  end

  def execute
    return existing_vote if existing_vote.present?

    vote = ::Vote.new(voteable: comment)
    vote.account = account

    if vote.save
      # Stories::BroadcastChanges.run! story: story
    else
      errors.merge!(vote.errors)
    end

    vote
  end

  private

  def existing_vote
    @existing_vote ||= ::Vote.find_by(voteable: comment, account: account)
  end
end
