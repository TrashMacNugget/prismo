class Comments::Create < ActiveInteraction::Base
  interface :parent
  string :body
  object :account

  validates :body, presence: true

  def to_model
    Comment.new
  end

  def execute
    comment = parent.is_a?(Story) ? parent.comments.new : parent.children.new
    story   = parent.is_a?(Story) ? parent : parent.story

    comment.body = body
    comment.story_id = story.id
    comment.account = account

    if comment.save
      Comments::Vote.run(comment: comment, account: account)

      after_comment_create(comment)
    else
      errors.merge!(comment.errors)
    end

    comment
  end

  private

  def after_comment_create(comment)
    comment.cache_depth
    comment.cache_body

    Comments::BroadcastCreation.run! comment: comment
  end
end
