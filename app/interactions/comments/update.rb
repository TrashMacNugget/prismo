class Comments::Update < ActiveInteraction::Base
  object :comment

  string :body

  def execute
    comment.body = body if body?

    if comment.save
      after_comment_save_hook(comment)
    else
      errors.merge!(comment.errors)
    end

    comment
  end

  private

  def after_comment_save_hook(comment)
    comment.cache_body
    Comments::BroadcastChanges.run! comment: comment
  end
end
