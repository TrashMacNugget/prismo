class StoryDecorator < Draper::Decorator
  delegate_all

  def domain
    return nil if !object.url.present?
    URI.parse(object.url).host
  end

  def score
    popularity + recentness
  end

  def popularity(weight: 3)
    object.votes_count * weight
  end

  def recentness(epoch: 1.day.ago.to_i, divisor: 3600)
    seconds = object.created_at.to_i - epoch
    (seconds / divisor).to_i
  end

end
