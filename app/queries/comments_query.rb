class CommentsQuery
  attr_reader :relation

  def initialize(relation = Comment.all)
    @relation = relation
  end

  def all
    relation.includes(:story, :account)
  end
end
