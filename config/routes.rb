Rails.application.routes.draw do
  use_doorkeeper

  devise_for :users, path: 'auth', controllers: {
    # omniauth_callbacks: 'auth/omniauth_callbacks',
    # sessions:           'auth/sessions',
    registrations:      'auth/registrations',
    # passwords:          'auth/passwords',
    # confirmations:      'auth/confirmations',
  }

  root to: 'application#app'

  get '.well-known/host-meta', to: 'well_known/host_meta#show', as: :host_meta, defaults: { format: 'xml' }
  get '.well-known/webfinger', to: 'well_known/webfinger#show', as: :webfinger

  resources :accounts, path: 'users', only: [:show], param: :username

  namespace :api do
    # Salmon
    post '/salmon/:id', to: 'salmon#update', as: :salmon

    namespace :v1 do
      resources :comments, only: [:index, :show, :update] do
        resource :vote, only: :create
        post :unvote, to: 'votes#destroy'

        resources :comments, controller: :comments, only: [:index, :create] do
          get :all, on: :collection
        end
      end

      resources :accounts, only: [:show]
      resources :tags, only: [:index, :show]

      resources :stories do
        post :scrap, on: :member

        resource :vote, only: :create
        post :unvote, to: 'votes#destroy'

        resources :comments, controller: :story_comments, only: [:index, :create] do
          get :all, on: :collection
        end
      end

      post 'tools/scrap_url', controller: :tools, action: :scrap_url
    end
  end

  namespace :settings do
    resource :profile, only: [:show, :update]
  end

  namespace :admin do
    resource :settings, only: [:edit, :update]
  end

  get '*path', to: 'application#app'
end
