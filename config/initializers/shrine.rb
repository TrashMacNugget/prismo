require "shrine"
require "shrine/storage/file_system"

Shrine.plugin :activerecord
Shrine.plugin :logging, logger: Rails.logger
Shrine.plugin :backgrounding
Shrine.plugin :cached_attachment_data
Shrine.plugin :restore_cached_data
Shrine.plugin :pretty_location
Shrine.plugin :remove_attachment
Shrine.plugin :default_url

Shrine.storages = {
  cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"),
  store: Shrine::Storage::FileSystem.new("public", prefix: "uploads/store"),
}

Shrine::Attacher.promote { |data| Shrine::PromoteJob.perform_later(data) }
Shrine::Attacher.delete { |data| Shrine::DeleteJob.perform_later(data) }

Shrine::Attacher.default_url do |options|
  "/placeholders/#{name}.jpg"
end
