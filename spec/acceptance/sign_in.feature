Feature: Signing in
  Background:
    Given i have an active account

  Scenario: valid sign in
    When I enter a sign-in page
    And fill the sign in form with valid data
    Then i should be signed in

  Scenario: invalid sign in
    When I enter a sign-in page
    And fill the sign in form with invalid data
    Then i should see a validation error
