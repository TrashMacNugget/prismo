Feature: Signing up
  @javascript
  Scenario: valid sign up data
    When I enter a sign-up page
    And fill the sign up form with valid data
    # Then i should see a message about confirmation email
    And i should be able to confirm my account by clicking a link in received email
    And i should be able to sign in to my account

  Scenario: invalid sign up data
    When I enter a sign-up page
    And fill the sign up form with invalid data
    Then i should see a validation errors
