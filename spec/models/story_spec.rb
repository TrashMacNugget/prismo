require "rails_helper"

describe Story, type: :model do
  # These two needs to be commented out as they are freezing the specs suite
  # (WTF ?!). @todo: take care of it somehow
  # it { is_expected.to have_many(:comments) }
  # it { is_expected.to belong_to(:url_meta) }

  it { is_expected.to belong_to(:account) }
  it { is_expected.to have_many(:votes) }
  it { is_expected.to have_many(:group_stories) }
  it { is_expected.to have_many(:groups) }
end
