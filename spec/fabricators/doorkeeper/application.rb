Fabricator(:doorkeeper_application, from: 'Doorkeeper::Application') do
  name 'App name'
  redirect_uri Doorkeeper.configuration.native_redirect_uri
  scopes Doorkeeper.configuration.scopes
end
