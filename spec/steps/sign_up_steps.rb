step "I enter a sign-up page" do
  visit new_user_session_path
  click_link 'Sign up'
end

step "fill the sign up form with valid data" do
  fill_in 'Username', with: 'test'
  fill_in 'Email', with: 'email@example.com'
  fill_in 'Password', with: 'TestPass'
  fill_in 'Password confirmation', with: 'TestPass'
  click_on 'Sign up'
end

step "fill the sign up form with invalid data" do
  fill_in 'Username', with: 'test'
  fill_in 'Email', with: 'email'
  fill_in 'Password', with: 'TestPass'
  fill_in 'Password confirmation', with: 'TestPassx'
  click_on 'Sign up'
end

step "i should see a validation errors" do
  expect(page).to have_content "Email is invalid"
  expect(page).to have_content "Password confirmation doesn't match Password"
end

# step "i should see a message about confirmation email" do
#   expect(page).to have_content('Please check your email inbox')
# end

step "i should be able to confirm my account by clicking a link in received email" do
  open_email('email@example.com')
  current_email.click_link 'Confirm my account'
  expect(page).to have_content 'Your email address has been successfully confirmed.'
  expect(current_path).to eq new_user_session_path
end

step "i should be able to sign in to my account" do
  fill_in 'Email', with: 'email@example.com'
  fill_in 'Password', with: 'TestPass'
  click_on 'Log in'

  expect(current_path).to eq '/'
end
