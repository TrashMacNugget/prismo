step "i have an active account" do
  @user = Fabricate(:user, confirmed_at: Time.zone.now)
end

step "I enter a sign-in page" do
  visit new_user_session_path
end

step "fill the sign in form with valid data" do
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: 'TestPass'
  click_on 'Log in'
end

step "fill the sign in form with invalid data" do
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: 'Wrong Pass'
  click_on 'Log in'
end

step "i should be signed in" do
  expect(current_path).to eq '/'
end

step "i should see a validation error" do
  expect(page).to have_content "Invalid Email or password"
end
