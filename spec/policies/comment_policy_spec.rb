require "rails_helper"

describe CommentPolicy do
  subject { described_class }

  let(:user) { Fabricate(:user) }
  let(:admin) { Fabricate(:user_admin) }
  let(:account) { Fabricate(:account, user: user) }
  let(:story) { Fabricate(:story) }
  let(:comment) { Fabricate(:comment, account: account, story: story) }

  shared_examples 'is prohibited for guests' do
    it { expect(subject).to_not permit(nil, comment) }
  end

  shared_examples 'is available for guests' do
    it { expect(subject).to permit(nil, comment) }
  end

  shared_examples 'is available for any non-guest user' do
    it { expect(subject).to permit(Fabricate(:user), comment) }
  end

  shared_examples 'is prohibitet for user other than comment author' do
    it { expect(subject).not_to permit(Fabricate(:account).user, comment) }
  end

  shared_examples 'is available for comment author' do
    it { expect(subject).to permit(comment.account.user, comment) }
  end

  shared_examples 'is available for admin' do
    it { expect(subject).to permit(admin, comment) }
  end

  permissions :index? do
    it_behaves_like 'is available for guests'
    it_behaves_like 'is available for any non-guest user'
  end

  permissions :create? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
  end

  permissions :update? do
    it_behaves_like 'is available for comment author'
    it_behaves_like 'is prohibitet for user other than comment author'
    it_behaves_like 'is available for admin'
  end

  permissions :comment? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
  end
end
